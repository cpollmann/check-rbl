# Check RBL

A commandline tool for checking if an ip address is listed on specific DNS Realtime Blacklists (RBL).


## Installation
Make sure you have at least Go version `1.20` installed.

Older versions of Go will probably work but are not tested.

You can get Go [here](https://go.dev/doc/install) or via your paketmanager.

1. Clone the repository
2. `cd` into the directory
3. run `go build `

## Usage
```
Usage of ./check-rbl:
  -c string
    	File with one RBL provider per line (default "/etc/rbl-provider")
  -i string
    	Valid IPv4 or IPv6 address to check against RBL providers (default "127.0.0.1")
```

## ToDo
- Option to enable Nagios compatible output
- Possibility to set a timeouts
- Multithread 

## License
MIT
