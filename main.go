package main

import (
	"flag"
	"log"
	"net"
	"os"
	"strings"
	"time"

	"github.com/seancfoley/ipaddress-go/ipaddr"
)

func reverseIPv4(ip string) string {
	octets := strings.Split(ip, ".")
	var revOctet = []string{octets[3], octets[2], octets[1], octets[0]}
	return strings.Join(revOctet, ".") + "."
}

func reverseIPv6(ip string) string {
	expandedIPv6 := ipaddr.NewIPAddressString(ip).GetAddress().ToFullString()
	var reversed string
	for _, v := range expandedIPv6 {
		reversed = string(v) + reversed
	}

	cleanIPv6 := strings.ReplaceAll(reversed, ":", "")
	var result string
	for _, v := range cleanIPv6 {
		result = result + string(v) + "."
	}
	return result

}

func checkValidIPv4(ip net.IP) bool {
	if ip.To4() != nil {
		return true
	} else {
		return false
	}
}

func checkValidIPv6(ip net.IP) bool {
	if ip.To16() != nil {
		return true
	} else {
		return false
	}
}

func parseIPAddress(ip string) string {
	ipAddress := net.ParseIP(ip)
	if checkValidIPv4(ipAddress) {
		return reverseIPv4(ipAddress.To4().String())
	} else if checkValidIPv6(ipAddress) {
		return reverseIPv6(ipAddress.To16().String())
	} else {
		log.Fatalf("Supplied IP address %s seems no valid", ip)
	}
	return "bla"
}

func parseRBLProvider(f string) []string {
	data, err := os.ReadFile(f)
	if err != nil {
		log.Fatalf("Could read file %s with error %s", f, err)
	}
	providers := strings.Split(string(data), "\n")
	return providers
}

func lookupList(h string, p []string) []string {
	var result []string
	for _, v := range p {
		result = append(result, h+v)
	}
	return result
}
func main() {
	var ipAddress = flag.String("i", "127.0.0.1", "Valid IPv4 or IPv6 address to check against RBL providers")
	var providerConfig = flag.String("c", "/etc/rbl-provider", "File with one RBL provider per line")
	//var nagiosOutput = flag.Bool("n", false, "Make output nagios compatible")
	//var timeout = flag.Int("t", 3, "Timeout per request")
	//var threads = flag.Int("p", runtime.NumCPU()/2, "Count of parallel requests that will be made")
	flag.Parse()

	hostList := lookupList(parseIPAddress(*ipAddress), parseRBLProvider(*providerConfig))

	for _, v := range hostList {
		time.Sleep(1000)
		addrs, err := net.LookupHost(v)
		if err != nil {
			log.Printf("%s is not marked as spam", v)
		} else {
			log.Printf("%s is listed with results: %s", v, addrs)
		}

	}
}
