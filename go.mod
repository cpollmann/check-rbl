module gitlab.com/cpollmann/check-rbl

go 1.20

require github.com/seancfoley/ipaddress-go v1.5.5

require github.com/seancfoley/bintree v1.2.3 // indirect
